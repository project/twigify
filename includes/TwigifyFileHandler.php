<?php

/**
 * @file
 * Class TwigifyFileHandler.
 *
 * Simple wrappers for file handling processes.
 */

class TwigifyFileHandler {

  // The filename of the current file.
  protected $fileName;
  // The path to the file with the file name appended.
  protected $filePath;
  protected $fileDirectory;
  protected $mappedDirectoryStructure;
  // ID for an open file resource.
  protected $fr;
  protected $fileData;
  protected $fileExt;
  protected $fError;

  /**
   * Initialises the TwigifyFileHandler class, setting path, name, and extension.
   *
   * This assumes that a filename is at the end of the filepath.
   *
   * @param string $file_path
   *   Path to file we wish to manipulate.
   */
  public function init($file_path) {
    $this->setFilePath($file_path);
    $this->setFilenameFromFilepath();
    $this->setFileExt($this->getExtension());
  }

  /**
   * Writes file buffer and closes files on object destruction.
   */
  public function __destruct() {
    if (isset($this->fr) && $this->doesExist($this->getFilePath())) {
      // $this->writeFile();
      // $this->closeFile();
    }
  }

  /**
   * Gets the extension of a file.
   *
   * @param int $index
   *   Offset at which the search will begin.
   *
   * @return string
   *   Extension of the file given after the last "." character.
   *
   * @see strrpos()
   */
  public function getExtension($index = 1) {
    $position = strrpos($this->fileName, '.', $index);
    return substr($this->fileName, $position);
  }

  /**
   * Changes a files extension.
   *
   * @param string $old_extension
   *   Original extension, eg "tpl.php".
   * @param string $new_extension
   *   New extension, eg "twig".
   */
  public function replaceExtension($old_extension, $new_extension) {
    $this->setFileName(str_replace($old_extension, $new_extension, $this->fileName));
    $this->setFilePath(str_replace($old_extension, $new_extension, $this->filePath));
  }

  /**
   * Sets $this->file_name based on the referenced file path.
   */
  public function setFilenameFromFilepath() {
    $this->fileName = basename($this->filePath);
  }

  /**
   * Wrappers for typical file operations.
   */

  /**
   * Opens $this->file_path for writing.
   */
  public function openFile() {
    $this->fr = fopen($this->filePath, 'wb');
  }

  /**
   * Populates $this->file_data with the contents of $this->file_path.
   */
  public function extractFileContents() {
    $this->fileData = file_get_contents($this->filePath);
  }

  /**
   * Writes $this->file_data to the opened file in $this->fr.
   */
  public function writeFile() {
    if (isset($this->fr) && isset($this->fileData)) {
      fwrite($this->fr, $this->fileData);
    }
  }

  /**
   * Closes the open file handle referenced in $this->fr.
   */
  public function closeFile() {
    if (isset($this->fr)) {
      fclose($this->fr);
    }
  }

  /**
   * Opens file, writes data, then closes the handle.
   */
  public function saveFileData() {
    $this->openFile();
    $this->writeFile();
    $this->closeFile();
  }

  /**
   * Deletes the currently open file.
   */
  public function deleteFile() {
    $current_file = $this->getFilePath();
    if (is_readable($current_file)) {
      unlink($current_file);
    }
  }

  /**
   * Helper functions  -> not reliant on class variables.
   */

  /**
   * Makes sure file directory is writable.
   *
   * @param string $dir
   *   Directory to check.
   *
   * @return bool
   *   TRUE if the directory is writable.
   *
   * @throws Exception
   */
  public function chmodDirectory($dir) {
    closedir(opendir($dir));
    if (!chmod($dir, 0777)) {
      throw new Exception('Couldn\'t chmod 0777 – $dir');
    }
    return TRUE;
  }

  /**
   * Writes the content $this->file_data to $this->file_path.
   *
   * @return bool
   *   TRUE if the write was successful.
   *
   * @throws Exception
   */
  public function writeBuffer() {
    if (!$handle = @fopen($this->filePath, 'w')) {
      throw new Exception("Cannot open file " . $this->filePath);
    }
    if (@fwrite($handle, $this->fileData) === FALSE) {
      throw new Exception("Cannot write to file " . $this->filePath);
    }
    @fclose($handle);
    return TRUE;
  }

  /**
   * Creates a directory relative to the root of the current site root.
   *
   * @param string $directory_path
   *   Directory path to create, or create a new directory within.
   * @param string $directory_name
   *   (optional) Directory name to create in the directory specified.
   */
  public function createDirectory($directory_path, $directory_name = NULL) {
    $full_path = ($directory_name) ? $directory_path . '/' . $directory_name : $directory_path;
    if (!file_exists($full_path) && !is_dir($full_path)) {
      if (!mkdir($full_path)) {
        throw new Exception('Failed to make directory');
      }
      else {
        $this->setFileDirectory($full_path);
      }
    }
  }

  /**
   * Checks if a file or directory exists.
   *
   * @param string $full_path
   *   Full file path to check.
   *
   * @return bool
   *   TRUE if file exists; FALSE if not.
   */
  public function doesExist($full_path) {
    $result = is_dir($full_path);
    if (!$result) {
      $result = file_exists($full_path);
    }
    return $result;
  }

  /**
   * Lists files in a directory by extension.
   *
   * @param string $base_path
   *   Path to map.
   *
   * @return array
   *   Array of "flat" directory structure, segregated by extension.
   */
  public function mapDirectoryStructure($base_path = '.') {
    // Directories to ignore when listing output. Many hosts will deny PHP
    // access to the cgi-bin.
    $ignore = array('cgi-bin');
    // Open the directory to the handle $dh.
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($base_path, FilesystemIterator::SKIP_DOTS)) as $file) {
      if (!in_array($file->getFileName(), $ignore)) {
        $relative_path = str_replace($base_path, '', $file->getPathName());
        $relative_path = rtrim($relative_path, '/');
        $all_files['all'][] = $relative_path;
        // Put all file extensions in their separate sub array.
        $all_files[$file->getExtension()][] = $relative_path;
      }
    };
    // Close the directory handle.
    $this->setMappedDirectoryStructure($all_files);
  }

  /**
   * Recursively copies a directory and its files from one path to another.
   *
   * @param string $src
   *   Source directory.
   * @param string $dst
   *   Destination directory.
   */
  public function copyDirectory($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while (FALSE !== ($file = readdir($dir))) {
      if (($file != '.') && ($file != '..')) {
        if (is_dir($src . '/' . $file)) {
          $this->copyDirectory($src . '/' . $file, $dst . '/' . $file);
        }
        else {
          copy($src . '/' . $file, $dst . '/' . $file);
        }
      }
    }
    closedir($dir);
  }

  public function setFileDirectory($file_directory) {
    $this->fileDirectory = $file_directory;
  }

  public function getFileDirectory() {
    return $this->fileDirectory;
  }

  public function setFError($f_error) {
    $this->fError = $f_error;
  }

  public function getFError() {
    return $this->fError;
  }

  public function setFileData($file_data) {
    $this->fileData = $file_data;
  }

  public function getFileData() {
    return $this->fileData;
  }

  public function setFileName($file_name) {
    $this->fileName = $file_name;
  }

  public function getFileName() {
    return $this->fileName;
  }

  public function setFilePath($file_path) {
    $this->filePath = $file_path;
  }

  public function getFilePath() {
    return $this->filePath;
  }

  public function setFileExt($file_ext) {
    $this->fileExt = $file_ext;
  }

  public function getFileExt() {
    return $this->fileExt;
  }

  public function setMappedDirectoryStructure($mapped_directory_structure) {
    $this->mappedDirectoryStructure = $mapped_directory_structure;
  }

  public function getMappedDirectoryStructure() {
    return $this->mappedDirectoryStructure;
  }

}
