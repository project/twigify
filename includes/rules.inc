<?php

/**
 * @file
 * Lists rules for parsing PHP code into globs.
 */

$self_contained_rules = array(
  // Sigh; some themes have empty blocks (cough, garland, cough).
  'empty' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '',
  ),
  'empty;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '',
  ),
  // Comments.
  'T_COMMENT' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_COMMENT   => '',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{#%2$s%1$s#}',
  ),
  'T_DOC_COMMENT' => array(
    'pattern' => array(
      T_OPEN_TAG    => '<?php',
      T_DOC_COMMENT => '',
      T_CLOSE_TAG   => '?>',
    ),
    'format' => '{#%2$s%1$s#}',
  ),
  // Look for:
  // T_OPEN_TAG (T_PRINT|T_ECHO) [(] (T_VARIABLE|T_CONSTANT_ENCAPSED_STRING) [)]
  // [;] T_CLOSE_TAG
  // Ignore echo w/multiple arguments for now.
  'print $var' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_VARIABLE  => '',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'print $var;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_VARIABLE  => '',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s.%2$s }}',
  ),
  'print($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'print($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo $var' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_VARIABLE  => '',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo $var;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_VARIABLE  => '',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  // Print constant string and variants.
  'print const_string' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'print const_string;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'print(const_string)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'print(const_string);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo const_string' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo const_string;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo(const_string)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo(const_string);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  // Print t(constant string) and variants; ignore substitution by t for now.
  'print t(const_string)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'print t(const_string);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'print(t(const_string))' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'print(t(const_string));' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'echo t(const_string)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'echo t(const_string);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'echo(t(const_string))' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'echo(t(const_string));' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 't',
      T_OPEN_PARENTHESIS => '(',
      T_CONSTANT_ENCAPSED_STRING => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s|t }}',
  ),
  'print render($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s %2$s }}',
  ),
  'print render($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s.%2$s }}',  // eg. {{ page.header, but also handles content, so vsprintf wants to complain about missing arg }} 
  ),
  'print(render($var))' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s %2$s }}',
  ),
  'print(render($var));' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_PRINT     => 'print',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s %2$s }}',
  ),
  'echo render($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo render($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo(render($var))' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  'echo(render($var));' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ECHO      => 'echo',
      T_OPEN_PARENTHESIS => '(',
      T_STRING    => 'render',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %s }}',
  ),
  // Show/hide/unset a variable.
  'show($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'show',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% show(%s) %%}',
  ),
  'show($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'show',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ show(%s) }}',
  ),
  'hide($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'hide',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s|without(\'%2$s\') }}',
  ),
  'hide($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'hide',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{{ %1$s|without(\'%2$s\') }}', 
  ),
  'unset($var)' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'unset',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% unset(%s) %%}',
  ),
  'unset($var);' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_STRING    => 'unset',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% unset(%s) %%}',
  ),
  // Check for if (): elseif ():, else if ():, else:, and endif[;]  // this is the one. 
  'if (boolean_expression):' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_IF        => 'if',
      T_BOOLEAN_EXPR => '',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% if %s %%}',
  ),
  'elseif (boolean_expression):' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ELSEIF    => 'elseif',
      T_BOOLEAN_EXPR => '',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% elseif %1$s %%}',
  ),
  'else if (boolean_expression):' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ELSE      => 'else',
      T_IF        => 'if',
      T_BOOLEAN_EXPR => '',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% elseif %s %%}',
  ),
  'else:' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ELSE      => 'else',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% else %%}',
  ),
  'endif' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ENDIF     => 'endif',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% endif %%}',
  ),
  'endif;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ENDIF     => 'endif',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% endif %%}',
  ),
  // Check for foreach (): and endfor[;]
  'foreach ($var as $val):' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_FOREACH   => 'foreach',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_AS        => 'as',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% for %2$s in %1$s %%}',
  ),
  'foreach ($var as $key => $val):' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_FOREACH   => 'foreach',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE  => '',
      T_AS        => 'as',
      T_VARIABLE  => '',
      T_DOUBLE_ARROW => '=>',
      T_VARIABLE  => '',
      T_CLOSE_PARENTHESIS => ')',
      T_COLON     => ':',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% for %2$s, %3$s in %1$s %%}',
  ),
  'endforeach' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ENDFOREACH => 'endforeach',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% endfor %%}',
  ),
  'endforeach;' => array(
    'pattern' => array(
      T_OPEN_TAG  => '<?php',
      T_ENDFOREACH => 'endforeach',
      T_SEMICOLON => ';',
      T_CLOSE_TAG => '?>',
    ),
    'format' => '{%% endfor %%}',
  ),
);

$open_brace_rules = array(
  'if (boolean_expression) {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_IF         => 'if',
      T_BOOLEAN_EXPR => '',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% if %s %%}',
  ),
  '} elseif (boolean_expression) {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_CLOSE_BRACE => '}',
      T_ELSEIF     => 'elseif',
      T_BOOLEAN_EXPR => '',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% elseif %s %%}',
  ),
  '} else if (boolean_expression) {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_CLOSE_BRACE => '}',
      T_ELSE       => 'else',
      T_IF         => 'if',
      T_BOOLEAN_EXPR => '',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% elseif %1$s %%}',
  ),
  '} else {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_CLOSE_BRACE => '}',
      T_ELSE       => 'else',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% else %%}',
  ),
  'foreach ($var as $val) {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_FOREACH    => 'foreach',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE   => '',
      T_AS         => 'as',
      T_VARIABLE   => '',
      T_CLOSE_PARENTHESIS => ')',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% for %2$s in %1$s %%}',
  ),
  'foreach ($var as $key => $val) {' => array(
    'pattern' => array(
      T_OPEN_TAG   => '<?php',
      T_FOREACH    => 'foreach',
      T_OPEN_PARENTHESIS => '(',
      T_VARIABLE   => '',
      T_AS         => 'as',
      T_VARIABLE   => '',
      T_DOUBLE_ARROW => '=>',
      T_VARIABLE   => '',
      T_CLOSE_PARENTHESIS => ')',
      T_OPEN_BRACE => '{',
      T_CLOSE_TAG  => '?>',
    ),
    'format' => '{%% for %2$s, %3$s in %1$s %%}',
  ),
);

$close_brace_rules = array(
  '}' => array(
    'pattern' => array(
      T_OPEN_TAG    => '<?php',
      T_CLOSE_BRACE => '}',
      T_CLOSE_TAG   => '?>',
    ),
    // Will tr end to endif or endfor in apply_rules.
    'format' => '{%% end %%}',
  ),
  '};' => array(
    'pattern' => array(
      T_OPEN_TAG    => '<?php',
      T_CLOSE_BRACE => '}',
      T_SEMICOLON   => ';',
      T_CLOSE_TAG   => '?>',
    ),
    // Will tr end to endif or endfor in apply_rules.
    'format' => '{%% end %%}',
  ),
);
