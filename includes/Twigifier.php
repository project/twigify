<?php

/**
 * @file
 * Provide internal logic for twigifier conversions.
 */

require_once 'luthor.inc';

/**
 * Class provides conversion specific functionality for converting Drupal
 * PHPTemplate themes into "Twig" themes.
 */
class Twigifier {

  protected $oldThemeName;
  protected $newThemeName;
  protected $theme;
  protected $themeData;
  protected $themeInfo;
  protected $tpls;
  protected $templates;
  protected $oldThemeDirectory;
  protected $newThemeDirectory;
  protected $fileData;
  protected $newDirectoryStructure;

  /**
   * Initialise an object of the class, setting theme name, data and directory.
   *
   * @param array $theme_data
   *   An array of theme data as returned by list_themes().
   *
   * @see twigify_init_twigify_class()
   * @todo Why is this not __construct()?
   */
  public function init($theme_data) {
    $this->setOldThemeName($theme_data->name);
    $this->setThemeData($theme_data);
    $this->setOldThemeDirectory($theme_data->filepath);
  }

  /**
   * Takes tpls in theme data array and registers the path to the data array.
   */
  public function assembleTPLS() {
    $theme_data = $this->getThemeData();
    if (!empty($theme_data)) {
      foreach ($theme_data as $value) {
        $is_current_theme = (isset($value['path'])) ? substr_count($value['path'], $this->oldThemeName) : NULL;
        $is_template      = (isset($value['template'])) ? TRUE : FALSE;
        if ($is_template && $is_current_theme) {
          // drush_print('  ' . $key . ': ' );
          // drush_print('     ' . $value['template'] . '.tpl.php');
          $tpls[] = $value['path'] . '/' . $value['template'] . '.tpl.php';
        }
      }
      if (!empty($tpls)) {
        $this->setTpls($tpls);
      }
    }
  }

  /**
   * Finds the preprocess and process functions from $this->themeData.
   *
   * We needed a more robust solution to accommodate edgecases.
   *
   * @deprecated This is a legacy function that is not used.
   */
  public function assembleTemplateProcessors() {
    if (!empty($this->themeData)) {
      foreach ($this->themeData as $value) {
        $is_current_theme = substr_count($value['path'], $this->oldThemeName);
        if (isset($value['preprocess functions']) && $is_current_theme) {
          for ($i = 0; $i < count($value['preprocess functions']); $i++) {
            if (substr_count($value['preprocess functions'][$i], $this->oldThemeName)) {
              $templates['preprocess'][] = $value['preprocess functions'][$i];
            }
          }
        }
        if (isset($value['process functions']) && $is_current_theme) {
          for ($i = 0; $i < count($value['process functions']); $i++) {
            if (substr_count($value['process functions'][$i], $this->oldThemeName)) {
              $templates['process'][] = $value['process functions'][$i];
            }
          }
        }
      }
      if (!empty($templates)) {
        $this->setTemplates($templates);
      }
    }
  }

  /**
   * Creates a YAML formatted theme info string.
   *
   * Pinched from himerus's http://drupal.org/project/omega_tools
   *
   * @return string
   *   YAML format data definition of a theme.
   *
   * @see Twigifier::buildInfoFile()
   */
  public function assembleNewInfoFile() {
    $new_info_file    = $this->createInfoFile();
    $info_file_string = $this->buildInfoFile($new_info_file);
    return $info_file_string;
  }

  /**
   * Creates a YAML formatted libraries file string.
   * Doesn't need to be its own function. 
   *
   * @return string
   *   YAML format asset data 
   *
   * @see Twigifier::buildInfoFile()
   */
    public function assembleNewLibFile() {
      $new_lib_file    = $this->createLibFile();
      $lib_file_string = $this->buildLibFile($new_lib_file);
      return $lib_file_string;
  }

  /**
   * Creates a structured array containing key theme information.
   *
   * Pinched from himerus's http://drupal.org/project/omega_tools
   *
   * @see Twigifier::getThemeInfo()
   */
  public function createInfoFile() {
    $theme_info = $this->getThemeInfo();
    if (isset($theme_info)) {
      // Copy the array.
      $new_info_file = $theme_info;
      // Make changes.
      $new_info_file['name']        = $this->getNewThemeName();
      $new_info_file['type']     = 'theme';
      $new_info_file['description'] = "'New Drupal 8 Twig Theme created with the help of Twigify.'";
      // $new_info_file['base theme']  = $subtheme->base;
      $new_info_file['engine']     = 'twig'; // Is this still needed for Drupal 8?
      $new_info_file['version']    = 'VERSION';
      $new_info_file['core']       = '8.x';
      $new_info_file['libraries'] = array($this->getNewThemeName() . '/global-styling');
      // $new_info_file['screenshot'] = 'screenshot.png'; // @UM, is this needed in Drupal 8?

      return $new_info_file;
    }
    return FALSE;
  }

  /**
   * Creates a structured array containing theme assets.
   *
   * @see Twigifier::getThemeInfo()
   */
  public function createLibFile() {
    $new_lib_file = $this->getThemeInfo();
    if (isset($new_lib_file)) {      
      // Unset everything but stylesheets key. Alternately could slice the array. 
      unset($new_lib_file['name']);
      unset($new_lib_file['description']);
      unset($new_lib_file['base theme']);
      unset($new_lib_file['core']);
      unset($new_lib_file['engine']);
      unset($new_lib_file['datestamp']);
      unset($new_lib_file['features']);
      unset($new_lib_file['package']);
      unset($new_lib_file['project']);
      unset($new_lib_file['regions']);
      unset($new_lib_file['settings']);
      unset($new_lib_file['skins']);

      $version['version']='VERSION';
      $lib_data=array_merge($version,$this->array_rekey($new_lib_file,'stylesheets','css'));  
            
      return $lib_data;
    }
    return FALSE;
  }

  /**
   * Converts array of old .info file format theme data into YAML formatted string. 
   *
   * @param array $array
   *   Theme info, usually parsed from a .info file.
   * @param string $prefix
   *   (optional) Prefix for keys. Currently not implemented.
   *
   * @return string
   *   YAML format data definition of a theme.
   *
   * @see Twigifier::updateThemeInfo()
   */
  protected function buildInfoFile($array, $prefix = FALSE) {
    $info = '';

    //$array['version'] = 'VERSION';
    unset($array['datestamp']);
    unset($array['project']);
    unset($array['stylesheets']);

    foreach ($array as $key => $value) {
      if (is_array($value)) {
        // $this->buildInfoFile($value, ($prefix ? "{$prefix}[{$key}]") : $key);
        $info .= $key . ":\n";
        switch ($key) {
          case 'stylesheets':
            foreach ($value as $vkey => $vvalue) {
              $info .= "  " . $vkey . ":\n";
              foreach ($vvalue as $cvalue) {
                $info .= "   - " . $cvalue . "\n";
              }
            }
            break;

          case 'settings':
          case 'regions':
            foreach ($value as $ckey => $cvalue) {
              $info .= "   " . $ckey . ": '" . $cvalue . "'\n";
            }
            break;

          case 'libraries':
            foreach ($value as $ckey => $cvalue) {
              $info .= "  - " . $cvalue . "\n";
            }
            break;

        }
        // $info .= $this->buildInfoFile($value, ($prefix ? "{$prefix}[{$key}]" : key));
      }
      else {
        $info .= $key;
        $info .= ": " . $value . "\n";
      }
    }

    return $info;
  }

  /**
   * Builds libraries file string from old .info file array. 
   *
   * @param array $array
   *   Theme info, usually parsed from a .info file.
   * @param string $prefix
   *   (optional) Prefix for keys. Currently not implemented.
   *
   * @return string
   *   YAML format asset data.
   *
   * @see Twigifier::updateThemeInfo()
   */
  protected function buildLibFile($asset_data, $prefix = FALSE) {
    $libdata = '';
    $lib_data=array();
    $lib_data['']='global-styling:'."\n";

    //next($asset_data);
    foreach ($asset_data as $key => $value) {
      
      if (is_array($value)) {
        $libdata .= '  ' . $key . ":\n";
        switch ($key) {
          case 'css':
            foreach ($value as $vkey => $vvalue) {
              if($vkey=='all'){
                $vkey='theme';$colon=': {';$media='';$line="}\n";
                $libdata .= "    " . $vkey . ":\n";
                }
              if($vkey=='print'){
                $colon=': { media:';$media=$vkey;$line=" }\n";$vkey='';
                }
              foreach ($vvalue as $cvalue) {
                $libdata .= "     ".$cvalue . $colon . $media . $line;
              }
            }
            break;

        case 'scripts':
            foreach ($value as $ckey => $cvalue) {
              $info .= "   - " . $cvalue . "\n";
            }
            break;
  
        }
      }
      else {
        $libdata .= '  ' . $key;
        $libdata .= ": " . $value . "\n";
      }
    }

   $lib_data['next']=$libdata;
    
   return implode('',$lib_data);
  }

  /**
   * Needed to map array keys. Tell me again why this isn't core php?
   */
  public function array_rekey($array, $oldkey, $newkey)
  { 
    $rekey = array();   
    foreach( $array as $k => $v )
    { 
      if ($k==$oldkey) 
      { 
        $k = $newkey;
        $out[$k] = $v; 
        continue; 
    }
    $rekey[$k] = $v;
  }
  return $out;
  }

  /**
   * Updates the new theme's info by parsing the old .info file.
   *
   * Theme information includes anything that might be included in a .info file,
   * for example:
   *   - Name
   *   - Description
   *   - Package
   *   - Version
   *   - Core compatibility
   */
  public function updateThemeInfo() {
    $this->setThemeInfo(drupal_parse_info_file($this->getOldThemeDirectory() . '/' . $this->getOldThemeName() . '.info'));
  }

  /**
   * Takes the file contents (in PHP) and converts them to Twig.
   */
  public function convertToTwig() {
    // Read in some helper functions we need.
    // Grrrrrr: PHP functions always have global scope.
    // @todo make the "public" methods static methods of some class (and hide
    // the rest).
    // Get a twigified version the file contents.
    $result = TwigifyLuthor\php_string_to_twig_string($this->getFileData());

    // Stuff the twigified PHP back into the file.
    $this->setFileData($result);
  }
  
  /**
   * Segregates full filepaths of new theme assets.
   *
   * Files are sorted into sibling arrays with the extension as the array
   * identifier. The extension comes from the theme directory's structure,
   * so paths are preserved.
   *
   * Also, creates an additional array of "twig" files which is essentially any
   * "tpl" found in the theme layer and a label created with the new extension.
   */
  public function processThemeExtensions() {
    if (isset($this->newDirectoryStructure['php'])) {
      foreach ($this->newDirectoryStructure['php'] as $key => $value) {
        if (preg_match('/\.tpl\.php$/', $value)) {
          // Convert it to a twig format and add it as a new sub array.
          $this->newDirectoryStructure['tpl'][]  = $value;
          $this->newDirectoryStructure['twig'][] = str_replace('tpl.php', 'html.twig', $value);
          unset($this->newDirectoryStructure['php'][$key]);
        }
      }
    }
  }

  public function setTpls($tpls) {
    $this->tpls = $tpls;
  }

  public function getTpls() {
    return $this->tpls;
  }

  public function setTemplates($templates) {
    $this->templates = $templates;
  }

  public function getTemplates() {
    return $this->templates;
  }

  public function setNewThemeName($new_theme_name) {
    $this->newThemeName = $new_theme_name;
  }

  public function getNewThemeName() {
    return $this->newThemeName;
  }

  public function setOldThemeName($old_theme_name) {
    $this->oldThemeName = $old_theme_name;
  }

  public function getOldThemeName() {
    return $this->oldThemeName;
  }

  public function setTheme($theme) {
    $this->theme = $theme;
  }

  public function getTheme() {
    return $this->theme;
  }

  public function setThemeData($theme_data) {
    $this->themeData = $theme_data;
  }

  public function getThemeData() {
    return $this->themeData;
  }

  public function setNewThemeDirectory($new_theme_directory) {
    $this->newThemeDirectory = $new_theme_directory;
  }

  public function getNewThemeDirectory() {
    return $this->newThemeDirectory;
  }

  public function setFileData($file_data) {
    $this->fileData = $file_data;
  }

  public function getFileData() {
    return $this->fileData;
  }

  public function setThemeInfo($theme_info) {
    $this->themeInfo = $theme_info;
  }

  public function getThemeInfo() {
    return $this->themeInfo;
  }

  public function setOldThemeDirectory($old_theme_directory) {
    $this->oldThemeDirectory = $old_theme_directory;
  }

  public function getOldThemeDirectory() {
    return $this->oldThemeDirectory;
  }

  public function setNewDirectoryStructure($new_directory_structure) {
    $this->newDirectoryStructure = $new_directory_structure;
  }

  public function getNewDirectoryStructure() {
    return $this->newDirectoryStructure;
  }

}
