<?php
/**
 * @file
 * Drush command file for Twigify module.
 */

/**
 * Implements hook_drush_command().
 */
function twigify_drush_command() {
  $items = array();

  // The 'twigify' command.
  $items['twigify'] = array(
    'description' => "Converts a non-Twig theme into a Twig theme.",
    'arguments'   => array(
      'theme' => 'Enter the machine name of the theme if you want to run through twigify',
    ),
    'options'     => array(
      'spreads' => array(
        'description'   => 'Comma delimited list of themes.',
        'example-value' => 'zen, omega',
      ),
    ),
    'examples'    => array(
      'drush twigify zen',
    ),
    'aliases'     => array('tw8'),
    // No bootstrap at all.
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['twigify-usage'] = array(
    'description'        => 'Instructions on how to twigify a Theme',
    'hidden'             => TRUE,
    'topic'              => TRUE,
    'bootstrap'          => DRUSH_BOOTSTRAP_DRUSH,
    'callback'           => 'drush_print_file',
    'callback arguments' => array(dirname(__FILE__) . '/twigify-topic.txt'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 */
function twigify_drush_help($section) {
  switch ($section) {
    case 'drush:twigify':
      return dt("Add help information here");

    case 'meta:twigify:title':
      return dt("twigify commands");

    case 'meta:twigify:summary':
      return dt("Add a twigify summary here.");
  }
}

/**
 * Begins an interactive shell, walking the user through a Twig conversion.
 *
 * @see drush_invoke()
 * @see drush.api.php
 */
function drush_twigify() {

  drush_print(twigify_logo());
  drush_print('- - - - - - - - -');
  drush_print("Welcome! ");
  drush_print(" ");
  drush_print("Before we begin, let's make sure you have *everything* that you need: ");
  drush_print(" 1. The name of your new theme");
  drush_print(" 2. The source theme to convert from installed on this Drupal instance.");
  drush_print(" 3. The UNIX root path to where you want the converted theme to be saved to.");
  drush_print(" ");
    
  // Not using drush_confirm() bc it doesn't allow for a default value
  if (drush_prompt("Do you have  / know the information above and want to continue? (y/n)", 'y') == 'y') 
  {
    drush_print('- - - - - - - - -');
    // Set up theme and necessary classes.
    $theme_object = twigify_select_theme();
    $twig         = twigify_init_twigify_class($theme_object);
    $fh           = new TwigifyFileHandler();
    drush_print('###### ' . $twig->getOldThemeName() . ' is the source theme. ######');

    // Walk through the steps.
    drush_print(" ");
    drush_print("Create a New Theme Name ");
    drush_print(" ");
    twigify_process_new_theme_name($twig);
    drush_print(" ");
    drush_print("Create a New Theme Directory ");
    drush_print(" ");
    twigify_create_new_theme_directory($twig, $fh);
    twigify_copy_theme_to_new_theme_directory($twig, $fh);
    drush_print(" ");
    drush_print("Create a New Theme Info File ");
    drush_print(" ");
    twigify_create_info_file($twig, $fh);
    drush_print(" ");
    drush_print("Process the TPLs");
    drush_print(" ");
    twigify_process_tpls($twig, $fh);
    drush_print(" ");
    drush_print("Process the Theme Functions ");
    drush_print(" ");
    twigify_process_theme_functions($twig, $fh);
    drush_print("Create Theme File ");
    drush_print(" ");
    twigify_create_theme_file($twig, $fh);
    drush_print(" ");

    drush_print('** REMEMBER **');
    drush_print('drush cc all and drush cron to see changes in your new theme layer!');
  }
  
  else {
    drush_print("When you have the info, comeback and try again!");
  } 
  
  // Finish up.
  drush_print(twigify_exit_message());
  drush_print('- - - - - - - - -');
}

/**
 * Provides argument values for shell completion.
 *
 * Command argument complete callback.
 *
 * @return array
 *   Array of popular fillings.
 */
function twigify_theme_complete() {
  return array('values' => array('default', 'zen'));
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_twigify_validate() {
  if (drush_is_windows()) {
    // $name = getenv('USERNAME');
    // @todo implement check for elevated process using w32api, as sudo is not
    // available for Windows.
    // http://php.net/manual/en/book.w32api.php
    // http://social.msdn.microsoft.com/Forums/en/clr/thread/0957c58c-b30b-4972-a319-015df11b427d
  }
  else {
    // Check to see if passed theme is installed.
  }
}
