<?php
/**
 * @file
 * Twigifier steps.
 *
 * Provides logic to convert an existing module's theme hooks to Twig.
 */

/**
 * Defines a nice list of steps needed for the conversion.
 *
 * @todo Return definitions.
 */
function twigify_checklistapi_checklist_info() {
  $definitions = array();
  $definitions['twigify'] = array(
    '#title' => t('Twigifier steps'),
    '#path' => 'admin/config/twigify/steps',
    '#description' => t('This provides the necessary steps to Twigify a Drupal module.'),
    '#help' => t('<p>Based on <a href="https://docs.google.com/document/d/1zf7LOdms2x9iqA7b_9vMxw540vg1tT_9VOcmqvyx3BY/edit#heading=h.wk68stavsugi">Drupal Twig conversion instructions</a>'),
    // Install Drupal 8.
    'install_drupal' => array(
      '#title' => t('Install Drupal 8'),
      'clone_drupal' => array(
        '#title' => t('git clone -b 8.x http://git.drupal.org/project/drupal.git d8'),
        '#description' => t('The current working version of Drupal will be installed in the folder d8 or name it whatever you like.'),
      ),
      'uncomment_twig' => array(
        '#title' => t('Uncomment all 3 Twig settings (debugging, cache, auto_reload) in settings.php.'),
      ),
      'drupal_normal' => array(
        '#title' => t('Install Drupal as normal'),
        '#description' => t('using the Standard install profile'),
      ),
      // Clone the Twig Sandbox:
      'clone_twig' => array(
        '#title' => t('Install Drupal 8'),
        'clone_sandbox' => array(
          '#title' => t('git clone -b front-end http://git.drupal.org/sandbox/pixelmord/1750250.git twigsandbox'),
          '#description' => t('The Twig sandbox version of Drupal will be installed in the folder twigsandbox or name it whatever you like'),
        ),
        'uncomment_twig' => array(
          '#title' => t('Uncomment all 3 Twig settings (debugging, cache, auto_reload) in settings.php.'),
          '#description' => t('The current working version of Drupal will be installed in the folder d8 or name it whatever you like.'),
        ),
        'drupal_normal' => array(
          '#title' => t('Install Drupal as normal'),
          '#description' => t('using the Standard install profile'),
        ),
        'switch_stark' => array(
          '#title' => t('switch_to_stark_theme'),
        ),
      ),
      // Ok, what do we convert?
      'choose conversion' => array(
        '#title' => t('Find a conversion issue, a module, or a theme you want to convert'),
      ),
      // Convert theme functions.
      'convert_' => array(
        '#title' => t('Convert a theme function to a template file & preprocess function:'),
      ),
      // Convert preprocess functions.
      'convert_preprocess' => array(
        '#title' => t('Convert or consolidate to preprocess functions'),
      ),
    ),
  );

  return $definitions;
}

/**
 * - Get all theme hooks in module
 * - Convert PHP templates to Twig templates
 * - Convert theme functions to Twig template + preprocess function
 * - which file was the theme function in?
 */


/**
 * Function stub.
 */
function twigify_checklistapi_checklist_info_alter(&$definitions) {

}

/**
 * Implements hook_form_alter().
 */
function twigify_form_checklistapi_checklist_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#checklist']->id == 'twigify') {

    $form['checklistapi']['notes'] = array(
      '#markup' => theme('item_list', array(
        'items' => array(
          t('Preprocess functions will replace all theme functions.'),
          t('If your template file has PHP logic in it that affects the variables that are being printed, that code will need to be moved to a preprocess function.
'),
          t('If your template began as a theme function, the theme function will need to be converted to a preprocess function.'),
          t('In the case that some theme functions already have related preprocess functions, the variable handling code in those theme functions needs to be moved into preprocess.'),
          t('Do not add a line to your hook_theme implementation telling Drupal to use a template file instead of a theme function.'),
        ),
      )),
    );
  }
}

/**
 * Invokes the Twigifier engine for conversion steps.
 */
function twigifier() {
  $theme_function_name = '';
  // @todo strip 'theme' prefix from function name.
  $template_name = $theme_function_name . 'html.twig';
  $template_file_name = str_replace('_', '-', $template_name . 'html.twig');
  if (!file_exists($template_file_name)) {
    file($template_file_name);
  }
}

/**
 * Automates conversion steps.
 */
function twigify_module_theme_hooks($module_to_twigify) {
  // @todo This is a function stub; implement.
}
