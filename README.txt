#################  TWIGIFY #################

Twigify converts Drupal 7 phpTemplate themes into Drupal 8 Twig themes

###Instructions###
###
1.  Download and install the Twigify module in a Drupal 7 installation. 2. 
Enable it. 3.  Add a Drupal 7 Theme that you would like to convert to your
'sites/all/themes' directory 4. CD to the root of the Drupal Installation 5.
drush cc all 6. drush cron 7. drush twigify 8. Follow the on screen instructions

###Dependencies###
Drush - Twigify currently runs as Drush command extension. Checklist API - Show
steps for theme conversion.

###Known Issues###
Tested on numerous themes including Bartik, Severn, Stark, Basic, and yes,
Bluemarine (Drupalcon Prague Twig Lab.) Twigify automatically converts these
themes to their D8 equivalents.

####No support for .inc preprocessing files####
Tested against base themes incl. Zen, Omega & Mothership. Functionality has not
been added to search for .inc that are preprocessing files - so Omega fails, as
does Mothership though not as spectacularly.

####Issue:"error: html_attributes could not be found in _context for
####maintenance-page.html.twig"####
####
Themes converted with Twigify may be affected by this issue (see
http://drupal.org/node/1885800.) This seems to be a core Drupal issue, please
check that thread and comment there accordingly before filing a similar issue in
the Twigify queue.

####Running Converted Theme in Drupal 8####
####
Otherwise in Drupal 8, you can enable the theme and see it running. Please
report any discovered issues not listed here in the issue queue.
